import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';


@Injectable({
  providedIn: 'root'
})
export class ApiServiceService {
  
  // Url  de la api
  url = 'http://localhost:8080/api';
  
  // Cors
  httpOptions = {
    headers: new HttpHeaders({        
      'Content-type': 'application/json',
      'Access-Control-Allow-Origin' : '*',
      'Access-Control-Allow-Methods': 'POST, GET, OPTIONS, PUT',
      'Accept':'application/json',
    })
  };


  constructor(private http: HttpClient) { }

  getAllAppointments(): Observable<any>{
    return this.http.get(this.url + '/citas',this.httpOptions);
  }

  getPets():  Observable<any>{
    return this.http.get(this.url+'/mascotas', this.httpOptions);
  }
  
  getAppointment(appointment): Observable<any>{
    return this.http.get(this.url + `/citas/${appointment}`,this.httpOptions);
  }

  getPet(pet): Observable<any>{
    return this.http.get(this.url + `/mascota/${pet}`,this.httpOptions);
  }

  deleteAppointmet(appointment): Observable<any>{
    return this.http.post(this.url + `citas/delete/${appointment}`,this.httpOptions);
  }
}
