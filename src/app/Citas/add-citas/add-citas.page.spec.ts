import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddCitasPage } from './add-citas.page';

describe('AddCitasPage', () => {
  let component: AddCitasPage;
  let fixture: ComponentFixture<AddCitasPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddCitasPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddCitasPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
